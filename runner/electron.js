const path = require('path')
const electron = require('electron')
const { spawn } = require('child_process')

function startElectron() {
  const electronProcess = spawn(electron, [path.join(__dirname, '../src/main/index.js')])

  electronProcess.stdout.on('data', (data) => {
    console.log('[Electron stdout start] =======')
    console.log(data.toString())
    console.log('[Electron stdout end] =========')
  })

  electronProcess.stderr.on('data', (data) => {
    console.log('[Electron stderr start] =======')
    console.log(data.toString())
    console.log('[Electron stderr end] =========')
  })

  electronProcess.on('close', () => {
    process.exit()
  })
}

process.env.RUNNER = 'electron'

startElectron()
