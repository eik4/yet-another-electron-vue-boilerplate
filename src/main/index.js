const electron = require('electron')

function createWindow () {
  const win = new electron.BrowserWindow({
    width: 1280,
    height: 720,
    webPreferences: {
      nodeIntegration: true
    }
  })

  if (process.env.RUNNER === 'dev') {
    win.loadURL('http://localhost:3000')
  }
  else if (process.env.RUNNER === 'electron') {
    win.loadFile('../../dist/index.html')
  }
  else {
    win.loadFile('./dist/index.html')
  }
  // win.webContents.openDevTools()
}

const app = electron.app

app.on('ready', () => {
  createWindow()
})

app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit()
  }
})
