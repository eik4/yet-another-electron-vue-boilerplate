import Vue from 'vue'
import Vuex from 'vuex'
import App from './components/App'
import storeModules from './store/index'
import './index.scss'

Vue.use(Vuex)

const store = new Vuex.Store(storeModules)

new Vue({
  el: '#app',

  store,

  components: {
    App
  },

  mounted() {
    this.$store.dispatch('domEventBus/init')
  },

  beforeDestroy() {
    this.$store.dispatch('domEventBus/reset')
  }
})
