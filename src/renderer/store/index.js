import domEventBus from './domEventBus'
import vueEventBus from './vueEventBus'

export default {
  modules: {
    domEventBus,
    vueEventBus
  }
}
