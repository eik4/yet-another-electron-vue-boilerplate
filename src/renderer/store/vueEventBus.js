const defaultState = {
  event: {}
}

export default {
  namespaced: true,

  state: () => ({
    ...defaultState
  }),

  mutations: {
    on(state, obj) {
      Object.entries(obj).forEach((entry) => {
        state.event[entry[0]] = entry[1]
      })
    },

    off(state, obj) {
      Object.entries(obj).forEach((entry) => {
        delete state.event[entry[0]]
      })
    },

    clearState(state) {
      Object.keys(defaultState).forEach((key) => {
        state[key] = defaultState[key]
      })
    }
  },

  actions: {
    emit(vuex, params) {
      const defaults = {
        name: 'none',
        arg: null
      }
      const options = { ...defaults, ...params }
      if (vuex.state.event[options.name] !== undefined) {
        vuex.state.event[options.name](options.arg)
      }
    }
  }
}
